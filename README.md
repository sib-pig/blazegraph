# Blazegraph Docker containerization

This project purpose is to have a Blazegraph (v. 2_1_5) server running on Java8 in a Docker container. 

The repository has a data folder where the .ttl data file should be placed before running the server.


**Get** the repository:

``git clone https://user@bitbucket.org/sib-pig/blazegraph.git``

**Move** to the repository:

``cd blazegraph``

**Build and run** the container as a deamon:

``docker-compose up -build -d``

**Access** the GUI on 
http://localhost:9999/

**Stop** the container:

``docker-compose down``

**Dependencies** :
* Docker https://docs.docker.com/get-docker/
* Docker Compose https://docs.docker.com/compose/install/