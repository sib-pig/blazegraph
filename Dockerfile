# based on https://bitbucket.org/sib-pig/sugarbind/src/develop/Dockerfile.rdf

# Use OpenJDK as base image
FROM openjdk:8-jre

# Change workdir to /app/
WORKDIR /app/

# Install Blazegraph
RUN wget https://github.com/blazegraph/database/releases/download/BLAZEGRAPH_RELEASE_CANDIDATE_2_1_5/bigdata.jar


# Copy data files
COPY ./data/ ./data/
COPY ./blazegraph.properties ./blazegraph.properties

# Expose port
EXPOSE 9999

# Load data from data folder into Blazegraph
RUN java -cp *:*.jar com.bigdata.rdf.store.DataLoader ./blazegraph.properties ./data/1_lib
RUN java -cp *:*.jar com.bigdata.rdf.store.DataLoader ./blazegraph.properties ./data/2_model
RUN java -cp *:*.jar com.bigdata.rdf.store.DataLoader ./blazegraph.properties ./data/3_data

# Launch Blazegraph
CMD ["java", "-server", "-Xmx2g", "-jar", "bigdata.jar"]
